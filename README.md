# Object-oriented programming assessment task

For this task you need to create a concept of a moving mechanical vehicle. Speaking in the most abstract terms of vehicle's capabilities, every kind of vehicle in existance can: accelerate, slow down, move around, while eventually some sorts of vehicles can have specific kind of capabilities, like movement only in specific directions, i.e. wheel-driven cars on the ground can move either left or right, while airplanes, missiles and rockets can move in all three-dimensions where they can also pitch, roll and yaw on their axis.

To complete this task successfully, you will need to create an object-oriented software setup in which will be easy to add new kinds of vehicles where they'll all have their common properties which we described above (acceleration, slowing down and directional movement).
For specific types of movement, i.e. three-dimensional movement, you'll need to use the same movement method and/or implmentation.

A mere example of what's to be done in psuedo-code:

```
class Aircraft ... {}

class TransportAircraft extends Aircraft {}

class AirbusA380 extends {
    public move(Movement move) {
        switch(move) {
            case MOVEMENT.PITCH:
                // calculate movement change
                break;
            case MOVEMENT.ROLL:
                // calculate movement change
                break;
            case MOVEMENT.YAW:
                // calculate movement change
                break;
        }
    }
}
```

Assume three-dimensional spacing for any kind of vehicle we're implementing. The initial position for all vehicles must be: `0, 0, 0`, being `X, Y, Z`.

**NOTE:** Collision detection for multiple vehicles is not a requirement for this task, but if achieved, it will be a great bonus to your application!

Further on, create an auto-wiring system for all implemented vehicles, so they can be executed during runtime in order.

For each vehicle, present the following:

```
Škoda VRS 2.0 2021 A8

Starting position: 0, 0, 0

Accelerated: 150
Moved left: 10
Accelerated: 150
Moved right: 30

Acceleration: 150
Movement:
    X: 150
    Y: 10

Acceleation: 150
Movement:
    X: 150
    Y: -30

New position: 300, -20, 0
```

From the example above the vehicle has moved `150` points forward, moving on the `Y` axis once `10` points, then moving the opposite way for `30` points, thus setting that position to `-20` on that axis.

**Development requirements:**

 - use TypeScript, C# or Java for this task
 - deploy on GitHub, GitLab or Bitbucket
 - consider creating this as a library / API to be reusable in other projects

**Special bonuses:**

 - web interface to present this
 - dockerization of web-related services
 - clean code and auto-generated documentation

**Concepts to consider:**

 - DTO
 - SOLID principles (Singleton, AbstractFactory, ...)
 - Design patterns
 - Newton's laws of motion
